# Double Pendulum

A douple pendulum simulation.

![a double pendulum](./out/examples/same-length-same-mass.webm)

## Setup

Just clone this repo, create a venv, install the packages and run it.

On linux:

    git clone https://gitlab.com/chrismit3s/double-pendulum
    cd double-pendulum
    python3.8 -m venv venv
    source ./venv/bin/activate
    python -m pip install -r requirements.txt

On windows (with [py launcher](https://docs.python.org/3/using/windows.html) installed):

    git clone https://gitlab.com/chrismit3s/double-pendulum
    cd double-pendulum
    py -3.8 -m venv venv
    .\venv\scripts\activate.bat
    python -m pip install -r requirements.txt

## Usage

Just run the `src` module with:

    python -m src [-h] [--fps FPS] [--res RES] [--preview-only] [-m1 M1] [-l1 L1] [-m2 M2] [-l2 L2] [--theta1 THETA1] [--theta2 THETA2] [--omega1 OMEGA1] [--omega2 OMEGA2]

| Argument       | Explanation |
|:---------------|:------------|
| -h, --help     | show this help message and exit |
| --fps          | the (maximum) framerate |
| --res          | the resolution |
| --preview-only | dont save the video, just show the preview |
| -m1            | the mass of the first pendulum |
| -l1            | the length of the first pendulum |
| -m2            | the mass of the second pendulum |
| -l2            | the length of the second pendulum |
| --theta1       | the starting angle of the first pendulum |
| --theta2       | the starting angle of the second pendulum |
| --omega1       | the starting angle-velocity of the first pendulum |
| --omega2       | the starting angle-velocity of the second pendulum |

## License

This project is licensed under the MIT License - see the LICENSE.md file for details
