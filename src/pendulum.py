import numpy as np
from collections import deque
from numpy import sin, cos
from src import PALETTE, GRAVITY, TIME_RATIO, TRACE_LEN


def _unit_vector(angle):
    # since the angle is not the the positive x, but to the negative y axis, sin and cos are swapped and the cos got a -
    return np.array([sin(angle), -cos(angle)], dtype="float")

def _angle_range(angle):
    return (angle + np.pi) % (2 * np.pi) - np.pi

def _area_to_radius(area):
    return np.sqrt(area / np.pi)


class Pendulum():
    def __init__(self, canvas, m1, m2, len1, len2, theta1, theta2, omega1=0, omega2=0):
        self.canvas = canvas
        self.masses = [m1, m2]
        self.lengths = [len1, len2]
        self.angles = [_angle_range(theta1), _angle_range(theta2)]
        self.vels = [omega1, omega2]
        self.trace = deque(maxlen=int(TRACE_LEN * self.canvas.fps))  # show the last TRACE_LEN seconds as a trace

    @property
    def rods(self):
        return [length * _unit_vector(angle) for length, angle in zip(self.lengths, self.angles)]

    @property
    def positions(self):
        rods = self.rods
        return [rods[0], rods[0] + rods[1]]

    def draw(self):
        self.canvas.fill(PALETTE["background"])

        # draw cross at origin
        x = min(self.canvas.size) // 50
        p1 = [-x, 0]
        p2 = [ x, 0]
        self.canvas.draw_line(p1, p2, PALETTE["cross"], thickness=2)
        self.canvas.draw_line(p1[::-1], p2[::-1], PALETTE["cross"], thickness=2)

        # draw trace
        self.canvas.draw_lines(self.trace, PALETTE["trace"], thickness=1)

        # draw rods
        positions = p1, p2 = self.positions
        for a, b in zip([(0, 0), p1], positions):
            self.canvas.draw_line(a, b, PALETTE["rod"], thickness=2)

        # draw masses
        for p, m in zip(positions, self.masses):
            self.canvas.draw_circle(p, _area_to_radius(m), PALETTE["mass"])

        self.canvas.update()

    def tick(self, dt):
        # update positions
        for i, v in enumerate(self.vels):
            self.angles[i] += v * dt

        # copy position of second mass for trace
        self.trace.append(np.array(self.positions[1]))

        # abbreviations
        m1, m2 = self.masses
        l1, l2 = self.lengths
        o1, o2 = self.angles
        w1, w2 = self.vels

        # update velocities (from https://www.myphysicslab.com/pendulum/double-pendulum-en.html)
        m = 2 * m1 + m2
        d = o1 - o2
        x = m - m1 * cos(2 * d)
        accs = [(-GRAVITY * m * sin(o1) - GRAVITY * m2 * sin(o1 - 2 * o2) - 2 * sin(d) * m2 * (w2**2 * l2 + w1**2 * l1 * cos(d))) / (l1 * x),
                (2 * sin(d) * (w1**2 * l1 * (m1 + m2) + GRAVITY * (m1 + m2) * cos(o1) + w2**2 * l2 * m2 * cos(d))) / (l2 * x)]

        for i, a in enumerate(accs):
            self.vels[i] += a * dt

    def loop(self):
        while self.canvas.is_open():
            self.draw()
            self.tick(TIME_RATIO / self.canvas.fps)

