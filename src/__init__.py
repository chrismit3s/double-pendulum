from os.path import join


GRAVITY = 9.8065

SCALE = 1.0  # in px/unit
TIME_RATIO = 2.5  # ratio simulation-time-steps/frame-time-steps
TRACE_LEN = 5  # in seconds

# see coolors.co/export/png/0b182a-50a2a7-e4d6a7-e9b44c-9b2915
PALETTE = {  # in bgr (expected by opencv)
    "background": (0x2A, 0x18, 0x0B),
    "cross":      (0xA7, 0xA2, 0x50),
    "trace":      (0xA7, 0xD6, 0xE4),
    "rod":        (0x4C, 0xB4, 0xE9),
    "mass":       (0x15, 0x29, 0x9B)}
OUT_DIR = join(".", "out", "")


from src.canvas import Canvas
from src.pendulum import Pendulum
