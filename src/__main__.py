from argparse import ArgumentParser
from os import remove
from src import Pendulum, Canvas
import numpy as np


if __name__ == "__main__":
    # setup args
    parser = ArgumentParser()

    # basic
    parser.add_argument("--fps",
                        type=float,
                        default=30.0,
                        help="the (maximum) framerate")
    parser.add_argument("--res",
                        type=str,
                        default="720x720",
                        help="the resolution")
    parser.add_argument("--preview-only",
                        dest="preview_only",
                        action="store_true",
                        help="dont save the video, just show the preview")

    # pendulums
    parser.add_argument("-m1",
                        type=float,
                        default=100,
                        help="the mass of the first pendulum")
    parser.add_argument("-l1",
                        type=float,
                        default=100,
                        help="the length of the first pendulum")
    parser.add_argument("-m2",
                        type=float,
                        default=100,
                        help="the mass of the second pendulum")
    parser.add_argument("-l2",
                        type=float,
                        default=100,
                        help="the length of the second pendulum")

    # starting config
    parser.add_argument("--theta1",
                        type=float,
                        help="the starting angle of the first pendulum")
    parser.add_argument("--theta2",
                        type=float,
                        help="the starting angle of the second pendulum")
    parser.add_argument("--omega1",
                        type=float,
                        help="the starting angle-velocity of the first pendulum")
    parser.add_argument("--omega2",
                        type=float,
                        help="the starting angle-velocity of the second pendulum")

    args = parser.parse_args()

    with Canvas(args.res.split("x"), args.fps) as canvas:
        p = Pendulum(canvas,
                     args.m1, args.m2,
                     args.l1, args.l2,
                     args.theta1 or np.random.uniform(np.pi / 2, np.pi),
                     args.theta2 or np.random.uniform(np.pi / 2, np.pi),
                     args.omega1 or 0,
                     args.omega2 or 0)
        p.loop()

    # delete file if wanted
    if args.preview_only or input("Save video? (Y/n) ").lower() == "n":
        remove(canvas.filename)
